// ==UserScript==
// @name         Roll20 QoL
// @namespace    https://gitlab.com/Steuls/roll20qol
// @version      1.0.2
// @description  Addition of various Roll20 QoL fixes and additional Features
// @author       Steuls
// @match        https://app.roll20.net/editor/
// @match        https://app.roll20.net/editor#*
// @match        https://app.roll20.net/editor?*
// @match        https://app.roll20.net/editor/
// @match        https://app.roll20.net/editor/#*
// @match        https://app.roll20.net/editor/?*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=roll20.net
// @resource     REMOTE_SEARCH_CSS https://gitlab.com/Steuls/roll20qol/-/raw/main/icon.css
// @resource     REMOTE_BASE_CSS https://gitlab.com/Steuls/roll20qol/-/raw/main/style.css
// @resource     REMOTE_OVERRIDES_CSS https://gitlab.com/Steuls/roll20qol/-/raw/main/overrides.css
// @resource     logIcon https://media.tenor.com/n6FM5Ny5e_wAAAAi/jp-james-perrett.gif
// @grant        GM_getResourceText
// @grant        GM_addStyle
// @grant        GM_log
// @grant        GM_getResourceURL
// @grant        unsafeWindow
// ==/UserScript==


// Load in base resources
const searchCss = GM_getResourceText("REMOTE_SEARCH_CSS");
GM_addStyle(searchCss);

const baseCss = GM_getResourceText("REMOTE_BASE_CSS");
GM_addStyle(baseCss);

const overrideCss = GM_getResourceText("REMOTE_OVERRIDES_CSS");
GM_addStyle(overrideCss);

const logImage = GM_getResourceURL("logIcon");

const logger = (message) => {
  const img = new Image();
  img.onload = function () {
    console.log(
      `%c+`,
      `
        font-size: 1px; 
        padding: 0 50px;
        line-height: 77px;
        background: url(${logImage});
        background-size: 100px 77px;
        color: transparent;`
    );
    console.log(message);
  };  
  img.src = logImage;
};

const generateSlot = (number) => {
  return `<div id="image${number}" class="tile" style="background-image: url('https://c.tenor.com/wXTO9bFFJXMAAAAC/loading-slow-internet.gif');"></div>`
};

const imageGrid = () => {
  let gridHTML = "";
  for (let i = 0; i < 9; i++) {
    gridHTML += generateSlot(i);
  };
  return gridHTML;
};

const header = `<div class="searchBox">
            <input id="gifsearchfield" class="searchInput"type="text" name="" placeholder="Search">
            <button id="gifsearchbutton" class="searchButton" href="#">
                <i class="material-icons">
                    search
                </i>
            </button>
        </div>`

const registerGifListeners = () => {
  for (let i = 0; i < 9; i++) {
    document.getElementById(`image${i}`).addEventListener("click", () => {
      if (unsafeWindow.d20 !== undefined) {
        logger("Clicked Gif: " + document.getElementById(`image${i}`).dataset.gif);
        unsafeWindow.d20.textchat.doChatInput(`[x](${document.getElementById(`image${i}`).dataset.gif}#.png)`);
      } else {
        logger("Roll20 QoL: d20 object not accessible!");
      }
    });
  }
}

const displayDiv = document.createElement("div");
displayDiv.id = "gifImageGrid";
displayDiv.classList.add("imageGrid");
displayDiv.innerHTML = header + imageGrid();

const togglePopup = (evt) => {
  if (evt.pointerId === undefined || evt.pointerId === -1) return
  const element = document.querySelector("#gifImageGrid");
  if (typeof(element) != "undefined" && element != null) {
    element.remove();
  } else {
    // Create Pop-up
    document.querySelector("#textchat-input").appendChild(displayDiv);
    registerGifListeners();
    document.querySelector("#gifsearchbutton").addEventListener("click", () => {
      grab_data(document.querySelector("#gifsearchfield").value);
    });
    document.querySelector("#gifsearchfield").addEventListener("keyup", (event) => {
      if (event.keyCode === 13) {
        grab_data(document.querySelector("#gifsearchfield").value);
      }
    });
    grab_data();
  }
}

const httpGetAsync = (theUrl, callback) =>
{
  // create the request object
  var xmlHttp = new XMLHttpRequest();

  // set the state change callback to capture when the response comes in
  xmlHttp.onreadystatechange = function()
  {
      if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
      {
          callback(xmlHttp.responseText);
      }
  }

  // open as a GET call, pass in the url and set async = True
  xmlHttp.open("GET", theUrl, true);

  // call send with no params as they were passed in on the url string
  xmlHttp.send(null);

}

// callback for the top 8 GIFs of search
const tenorCallback_search = (responsetext) =>
{
  // Parse the JSON response
  const response_objects = JSON.parse(responsetext);

  const top_9_gifs = response_objects["results"];

  top_9_gifs.forEach((gif, index) => {
    document.getElementById(`image${index}`).style.backgroundImage = `url(${gif.media_formats.nanogif.url})`;
    document.getElementById(`image${index}`).dataset.gif = `${gif.media_formats.nanogif.url}`;
  });
}


// function to call the trending and category endpoints
// search_term is optional if not provided show featured/trending
const grab_data = (search_term) =>
{
  // set the apikey and limit
  var apikey = "AIzaSyCxmGF9Y0EoREd751kjDcJTQy1c1ULe6Us";
  var clientkey = "chrome_extension";
  var lmt = 9;

  let search_url = "";

  if (search_term) {
    search_url = "https://tenor.googleapis.com/v2/search?q=" + search_term + "&key=" +
          apikey +"&client_key=" + clientkey + "&limit=" + lmt;
  } else {
    search_url = "https://tenor.googleapis.com/v2/featured?key=" +
          apikey +"&client_key=" + clientkey + "&limit=" + lmt;
  }
  httpGetAsync(search_url,tenorCallback_search);
}

//Creating Elements
const btn = document.createElement("button")
const text = document.createTextNode("Gifs");
btn.appendChild(text);
btn.onclick = togglePopup;
btn.classList.add("btn");

//Appending to DOM
document.querySelector("#textchat-input").appendChild(btn);
logger("Roll20 QoL UserScript has Initialised");
